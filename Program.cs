﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CarRace
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var numberOfCars = ReadNumber("Enter the number of cars for the race");
            var tasks = new Task[numberOfCars];
            
            if (numberOfCars <= 0)
            {
                Console.WriteLine("Very well then, no race.");
                return;
            }
            
            Console.WriteLine($"{numberOfCars} will race for 1000km with speeds of up to 100km/h");
            
            for (int i = 0; i < numberOfCars; i++)
            {
                var car = new Car(i+1, new SynchronizedRandomGenerator(0, 100), 10000);
                tasks[i] = Task.Factory.StartNew(() => car.Race());
            }
            Task.WaitAll(tasks);
            
            Console.WriteLine("\n\n\nThe race completed.\nThe program will exit in 30 seconds");
            Thread.Sleep(30000);
        }

        public static int ReadNumber(string message)
        {
            Console.WriteLine(message);
            while (true)
            {
                try
                {
                    var num = Convert.ToInt32(Console.ReadLine());
                    return num;
                }
                catch (System.Exception)
                {
                    Console.WriteLine("Invalid Input, please specify a number");
                }
            }
        }
    }
}
