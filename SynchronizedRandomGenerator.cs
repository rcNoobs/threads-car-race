using System;
public sealed class SynchronizedRandomGenerator
{
    private Random random {get; set;}
    private int minValue {get;}
    private int maxValue {get;}
    
    public SynchronizedRandomGenerator(int min, int max)
    { 
        random = new Random();
        minValue = min;
        maxValue = max;
    }
    public int Next()
    { 
        return random.Next(minValue, maxValue);
    }
}
