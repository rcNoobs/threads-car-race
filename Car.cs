using System;
using System.Threading;
public sealed class Car
{
    public int ID => id;
    private int id {get;}
    private int destination {get;}
    private SynchronizedRandomGenerator generator {get;}
    public Car(int carId, SynchronizedRandomGenerator randomGenerator, int destKm)
    { 
        id = carId;
        generator = randomGenerator;
        destination = destKm;
    }

    public void Race()
    {
        var distance = 0;
        while (distance < destination)
        {
            distance += generator.Next();
            Console.WriteLine($"Car: {id} - is at {distance} kms.");
            Thread.Sleep(10);
        }
        Console.WriteLine($"*************************************** Car {id} finished race.");
    }
}